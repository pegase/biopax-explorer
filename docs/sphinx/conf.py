import os
import sys

from urllib.parse import quote

sys.path.insert(0, os.path.abspath('./'))

#srcd="/doc/tmpxyz/src/biopax_explorer"
#srcd2="/doc/tmpxyz/"

# Add the path to the Python code directory to the system path
#sys.path.insert(0, os.path.abspath(srcd))
#sys.path.insert(0, os.path.abspath(srcd2))
 


# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

project = 'biopax-explorer'
copyright = '2024, Francois Moreews'
author = 'Francois Moreews'
release = '1.0'

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration



templates_path = ['_templates']
#exclude_patterns = ['_build', 'Thumbs.db', '.DS_Store']
exclude_patterns = []




# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = 'furo'

#html_theme = 'alabaster'
html_static_path = ['_static']


#source_dir=srcd

extensions = [
    'sphinx.ext.autodoc',
#    'sphinx.ext.napoleon',
    'sphinx_automodapi.automodapi',
    "sphinx.ext.autosummary",
    "sphinx.ext.intersphinx",  # Link to other project's documentation (see mapping below)
    "sphinx_autodoc_typehints",  # Automatically document param types (less noise in class signature)
    "numpydoc",
    "sphinx.ext.linkcode",
    #"sphinx.ext.githubpages",
    # Other enabled extensions...
]
autosummary_generate = False
autosummary_imported_members = False

source_suffix = '.rst'
# The name of the Pygments (syntax highlighting) style to use.
pygments_style = 'sphinx'

# If true, `todo` and `todoList` produce output, else they produce nothing.
todo_include_todos = False
#source_suffix = {
#    '.rst': 'restructuredtext',
#    '.txt': 'restructuredtext',
 #   '.md': 'markdown',
#    '.py': 'python',
#}

def linkcode_resolve(domain, info):
    # print(f"domain={domain}, info={info}")
    if domain != 'py':
        return None
    if not info['module']:
        return None
    inf=str(info)
    filename = quote(info['module'].replace('.', '/'))
    # if not filename.startswith("tests"):
    #    filename = "src/" + filename
    elem=filename.split("/")
    lt=elem[len(elem)-1]
    if  lt=="biopax" :
       filename=filename+"/"+lt+".py"
    if len(elem)>=3:
       filename=filename+"/"+lt+".py"
    if "fullname" in info:
        anchor = info["fullname"]
        fullname=info["fullname"]
        anchor = "#:~:text=" + quote(anchor.split(".")[-1])
    else:
        anchor = ""

    # git repo
    result = "https://forgemia.inra.fr/pegase/biopax-explorer/-/tree/main/src/%s#%s" % (filename,inf)
    # print(result)
    return result

 
