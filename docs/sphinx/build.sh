#!/bin/bash
echo "--start--: $PWD  $HOSTNAME"
ls -l 

cp conf.py  tmpxyz/src 
cp index.rst   tmpxyz/src 
cp -r _build tmpxyz/src/
cp -r _static tmpxyz/src/
cp -r _templates tmpxyz/src/
 
# 
python3.8  sphinx-build.py   -vvv tmpxyz/src/  web 
python3.8 format.py
echo "--end--"
