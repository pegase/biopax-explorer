import os
import fileinput
import shutil
import sys

def replace_string_in_files(directory, old_string, new_string):
    # Walk through all files and directories
    for root, dirs, files in os.walk(directory):
        for file in files:
            if file.endswith('.html') or file.endswith('.js')  :  # Process only text files
                filepath = os.path.join(root, file)
                # Read the content of the file
                with open(filepath, 'r') as f:
                    content = f.read()
                # Replace the string in the content
                modified_content = content.replace(old_string, new_string)
                # Write the modified content back to the file
                with open(filepath, 'w') as f:
                    f.write(modified_content)


 

def rename_files_and_directories(root_directory,fd,rp,matchdir):
    for root, dirs, files in os.walk(root_directory):
        print(dirs)
        for dd in dirs:
          rd="%s/%s"%(root,dd)
          if len(dd)>2 :
             rename_files_and_directories(rd,fd,rp,matchdir)
        #sys.exit()
        # Rename directories
        for directory in dirs:
            if fd in directory:
                old_path = os.path.join(root, directory)
                if matchdir in old_path :
                  new_directory_name = directory.replace(fd, rp)
                  new_path = os.path.join(root, new_directory_name)
                  #os.rename(old_path, new_path)
                  shutil.copytree(old_path, new_path)
                  print(f"copy directory: {old_path} to {new_path}")

        # Rename files
        for file_name in files:
            #print(file_name)
            if  fd in file_name:
                print(fd,"  ", file_name)
                old_path = os.path.join(root, file_name)
                #if matchdir in file_name :
                #
                
                #os.rename(old_path, new_path)
                new_file_name1 = file_name.replace(fd,rp)
                new_file_name1 = new_file_name1.replace("biopax_explorer","biopax-explorer")
                new_path1 = os.path.join(root, new_file_name1)
                if old_path!=new_path1:
                  shutil.copyfile(old_path, new_path1)
                  print(f"copy file: {old_path} to {new_path1}")
                new_file_name2 = file_name 
                new_file_name2 = new_file_name2.replace("biopax_explorer","biopax-explorer")
                new_path2 = os.path.join(root, new_file_name2)
                if old_path!=new_path2:
                  shutil.copyfile(old_path, new_path2)
                  print(f"copy file: {old_path} to {new_path2}")

# Example usage:
root_directory = './web/' 
 
 



replace_string_in_files(root_directory, 'biopax_explorer', 'biopax-explorer')

replace_string_in_files(root_directory, '_static/', '-static/')
replace_string_in_files(root_directory, '_images/', '-images/')

replace_string_in_files(root_directory, "documentation_options.js", "documentation-options.js")
replace_string_in_files(root_directory, "sphinx_highlight.js", "sphinx-highlight.js")
replace_string_in_files(root_directory, "language_data.js", "language-data.js")


rename_files_and_directories(root_directory,"_","-","_static")
rename_files_and_directories(root_directory,"_","-","api")
rename_files_and_directories(root_directory,"_","-","-static")
rename_files_and_directories(root_directory,"_","-","_images")
 