# BIOPAX-Explorer


## presentation

BIOPAX-Explorer is a Python package, designed to ease the manipulation of BIOPAX datasets.
 It exploits RDF and SPARQL, using a simple object-oriented, domain specific syntax.



Biological Pathway Exchange (BioPAX) is a standard language
 that aims to enable integration, exchange, visualization and analysis of biological pathway data. 

BioPAX is defined in OWL  and is represented in the RDF/XML format.
using these specifications and formats, BIOPAX-Explorer provides a 100 % compatible object-model 
with advanced query features, enabling quick analysis of datasets with a low learning curve.

## authors 

 F. Moreews  2022 2024 (main author & lead developper )
 J.B. Bougaud (intern)
 O. dameron (contributor)

## compatibility with the standards

the BIOPAX-Explorer object model follow the full BIOPAX level 3 specification. 
The conformity of the implementation is garantied by a code generation technic (owl to python) performed by the rdfobject package
- [ ] [OWL](http://www.biopax.org/release/biopax-level3.owl) 
- [ ] [code generator](https://gitlab.inria.fr/fmoreews/rdfobject)  

The library core code (biopax-explorer.biopax) is generated using RDFObject. 

## Example

```
###################################################
from biopax_explorer.pattern.pattern import PatternExecutor, Pattern
from biopax_explorer.query import  EntityNode
from biopax_explorer.biopax import *
import json 
    
##################################################
p=Pattern()
   
prot = EntityNode("PR", Protein())
prot.whereAttribute("name", "glucokinase","CONTAINS")
entityReference=EntityNode("EN", EntityReference())
prot.connectedWith(entityReference, "entityReference")
p.define(prot,entityReference)
 

pe=PatternExecutor()
pe.datasetFile("data/input/export_all_g6p.xml") 

 
result = pe.fetchEntities(p)
print("#automatic result mapping")
for entity_row in result[:1]:#only first result row
  for entity in entity_row:
      if isinstance(entity,Protein):
          print("first match of ",entity.meta_label," : ",entity.get_displayName(),",",entity.get_name())
          print( entity.to_json() )
```
output:
```
#automatic result mapping
first match of  PR  :  ADPGK , ADP-dependent glucokinase
{
  "uri": "http://www.reactome.org/biopax/56/71387#Protein186",
  "dataSource": {
    "__uri__": "http://www.reactome.org/biopax/56/71387#Provenance1",
    "comment": null,
    "xref": null,
    "displayName": null,
    "name": null,
    "standardName": null
  },
  "evidence": null,
  "xref": {
    "__class__": "Xref",
    "uri": "http://www.reactome.org/biopax/56/71387#UnificationXref_reactome_database_id_release_56_5696062"
  },
  "availability": null,
  "comment": "http://www.reactome.org/biopax/56/71387#Protein186http://www.reactome.org/biopax/56/71387#Complex122@Layout@http://chibecreated#1467726883224-1696734689@231@738",
  "displayName": "ADPGK",
  "name": "ADP-dependent glucokinase",
  "standardName": null,
  "cellularLocation": {
    "__class__": "CellularLocationVocabulary",
    "uri": "http://localhost:3030/g6p/CellularLocationVocabulary_b761180db8a874567188c589b45cab17"
  },
  "feature": {
    "__class__": "EntityFeature",
    "uri": "http://www.reactome.org/biopax/56/71387#FragmentFeature174"
  },
  "memberPhysicalEntity": null,
  "notFeature": null,
  "entityReference": {
    "__class__": "EntityReference",
    "uri": "http://identifiers.org/uniprot/Q9BRR6"
  },
  "__class__": "Protein"
}
```

```
#sparql code generation
querylist=PatternExecutor().queries(p)
print("#sparql code generation")
for q in querylist:
    print("#---generated sparql query---\n\n")
    print(q)

```
output:
```
#sparql code generation
#---generated sparql query---


prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
prefix bi: <http://www.biopax.org/release/biopax-level3.owl#>
select ?s2 ?o2 ?entityReferencen0E ?s2__rdft ?o2__rdft
where {
?s2 ?entityReferencen0E ?o2 .
?s2 rdf:type ?s2__rdft .
?o2 rdf:type ?o2__rdft .
{ ?o2 a bi:EntityReference }  UNION  { ?o2 a bi:RnaReference }  UNION  { ?o2 a bi:RnaRegionReference }  UNION  { ?o2 a bi:DnaReference }  UNION  { ?o2 a bi:ProteinReference }  UNION  { ?o2 a bi:SmallMoleculeReference }  UNION  { ?o2 a bi:DnaRegionReference }  .
{ ?s2 bi:name ?name }.
{ ?s2 a bi:Protein }
FILTER (  ?entityReferencen0E =  bi:entityReference  ) .
FILTER ( CONTAINS(?name,'glucokinase')  ) .
}
```

# installation


## Pip

```
pip install biopax-explorer
```
## Execute the Mini Introduction example:
```
# command line on Linux
git clone https://forgemia.inra.fr/pegase/biopax-explorer.git
cd biopax-explorer
python  script/intro.py  # using Python3.8 here
```

 - [mini intro code](https://fjrmoreews.github.io/biopax-explorer/tutorials/introduction.html)

## Docker 
docker : see docker and docker-compose install 
[docker-compose](https://docs.docker.com/compose/install/)
Linux (simple) but possible on Windows ans Mac
```
docker-compose up
# or  docker compose up #without '-', depending on the version
# or docker-compose -f ./docker-compose.yml up
```
The docker compose install allows a quick overview of the features by providing a jupyter notebook and a triple store (fuseki)


#### ready to use Docker container

We provide a easy to use Docker installation,tested with [Docker](https://www.docker.com/) on Linux and [Singularity](https://sylabs.io/)

The  Biopax-Explorer Docker container is available at [https://hub.docker.com/r/fjrmore/biopax_explorer](https://hub.docker.com/r/fjrmore/biopax_explorer)

The container can be used to deploy a jupyter enviroment with  tutorials and example notebooks

A easy way to test Biopax Explorer is to use docker compose :

```
# at the root of the repository
docker-compose up
#the triple store (fuseki) will be available on http://localhost:3030/  (login/password admin/admin)
#Jupyter will be available on http://localhost:8888/  (password : pass)
```

```
#contain of the docker-compose.yml file:

  version: '3.8'
  services:
   biopax-explorer:

    image: fjrmore/biopax_explorer

    volumes:
      - ./script/:/work/script
      - ./input:/work/input
    
    depends_on:
      - db
    ports:
      - "8888:8888"
    command: ["jupyter", "notebook", "--port=8888", "--no-browser", "--ip=0.0.0.0", "--allow-root","--NotebookApp.token='pass'" ]


   db:

    image: stain/jena-fuseki:4.0.0
 
    ports:
      - "3030:3030"
    volumes:
      - ./input/:/staging
      - ./db/fuseki:/fuseki
      - ./script/:/script
    environment: 
      - JVM_ARGS=-Xmx5024M
      - ADMIN_PASSWORD=admin


```



### optional high performance graph back-end
 The pip command installs [Networkx](https://networkx.org/) as a graph in memory back-end.

 If you want to use the [graph-tool](https://graph-tool.skewed.de/) package within biopax-explorer (graph layer), please follow the specific installation instruction.
Graph-tool is needed for large graph processing.


## quick start


tutorials and first step introduction are available as Jupyter notebooks :

 - [notebooks](https://fjrmoreews.github.io/biopax-explorer/tutorials/notebooks_main.html)

## benchmarks

[benchmarks](benchmarks/README.md)

## datasets

- [example datasets](https://gitlab.inria.fr/fmoreews/rdfobject/-/tree/master/release/script/bench/data/biopax?ref_type=heads)
  

