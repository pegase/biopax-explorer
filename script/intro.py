###################################################
from biopax_explorer.pattern.pattern import PatternExecutor, Pattern
from biopax_explorer.query import  EntityNode
from biopax_explorer.biopax import *
import json 
    
##################################################

p=Pattern()
   
prot = EntityNode("PR", Protein())
prot.whereAttribute("name", "glucokinase","CONTAINS")
entityReference=EntityNode("EN", EntityReference())
prot.connectedWith(entityReference, "entityReference")
p.define(prot,entityReference)
 

pe=PatternExecutor()
pe.datasetFile("script/data/input/export_all_g6p.xml") 

 
result = pe.fetchEntities(p)
print("#automatic result mapping")
for entity_row in result[:1]:#only first result row
  for entity in entity_row:
      if isinstance(entity,Protein):
          print("first match of ",entity.meta_label," : ",entity.get_displayName(),",",entity.get_name())
          print( entity.to_json() )

querylist=PatternExecutor().queries(p)
print("#sparql code generation")
for q in querylist:
    print("#---generated sparql query---\n\n")
    print(q)