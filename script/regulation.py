#!/usr/bin/env python
# coding: utf-8

# In[1]:


from biopax_explorer.pattern.rack import Rack, control_by_controlled, control_by_controller

from biopax_explorer.pattern.pattern import PatternExecutor
import json


from biopax_explorer.pattern.pattern import PatternExecutor, Pattern,PK
from biopax_explorer.query import  EntityNode
from biopax_explorer.biopax import *

from biopax_explorer.biopax.doc import helper

import json
import pickle
from graph_tool.all import *

import uuid


from typing import Iterable,List, Set
from urllib.parse import urlparse

from typing import Set, Collection
 
#from model_utils import ModelUtils
from urllib.parse import urlparse
import html
import re


# In[2]:


import logging as logger
logger.basicConfig(level=4, format='%(asctime)s - %(levelname)s - %(message)s')


# In[3]:


from typing import List, Union
 
def describeEntity(ent):
    ret=None
    clsn=ent.__class__.__name__
    dh2=helper.select(clsn)
    if dh2 is not None:
       ret=dh2.attributeNames()
    return ret

def count_pattern_search_result( element_list: list,silence=False) -> int:
    ####dbio_pax_element_matches: dict[BioPAXElement, List[Match]]
    verbose = False
    ct = 0
    occurencenb={}
    bio_pax_element_matches={}
    for row in element_list:
        for el in row:
           #print("------++",el) 
           #print("------",el.__class__.__name__)
           #k=None 
           if el.__class__.__name__=="PK" :                
               k=el.cls
               #print("-PK->"+str(el))
               
           else:
               #print("-NOTPK->",k,el) 
               k=el.__class__.__name__
           #print("key:"+k)
           oct=0    
           if el.pk in occurencenb.keys():
                  oct=occurencenb[el.pk]
           oct=oct+1
           occurencenb[el.pk]=oct 
           if oct==1:   # we count only unique uri   
             ct=0       
             if k in bio_pax_element_matches.keys():
                ct=bio_pax_element_matches[k]
             ct=ct+1       
             bio_pax_element_matches[k]=ct      
    
    #check_unique(occurencenb,silence)    
    match_display_info(bio_pax_element_matches,silence)
    return bio_pax_element_matches        
 

def check_unique(occurencenb, silence=False):
    # ement or replace with actual match display logic based on your requirements
    if silence==False:
      logger.debug("check unicity by classes by uri")
      for k, v in occurencenb.items():
        if v>1:
         logger.debug("warning occurence of %s  > 1 (%s)" %(k,v))        

#def match_display_info(p: Pattern, matches: List[Match]):
def match_display_info(bio_pax_element_matches, silence=False):
    # ement or replace with actual match display logic based on your requirements
    if silence==False:
      logger.debug("matches count by classes")
      for k, v in bio_pax_element_matches.items():
         logger.debug("%s=%s" %(k,v))        




def isOnlyModelInstance(obj,cls):
    #cc=obj.__class__.__name__
    #cln=cls.__name__
    if isinstance(obj,cls):
        return True
    else:
        return False
    
def isPKorInstance(obj,cls):

    r= False
    if isinstance(obj,cls):
        r= True
    else:
       cc=obj.__class__.__name__
       cln=PK.__name__
       if cc==cln:
          r= True 
           
    return r    

def cast(be, cls):
  # TODO :  needed?
    return be 


# In[4]:


def isTagged(ent,label,silent=True):
  r=False
  if silent is not True:
     logger.debug("meta:%s" %(ent.meta_label))
  if ent.meta_label is not None and ent.meta_label==label:
      r=True
  return r
    
class RegulationProcessor():
  def __init__(self,pe,g) -> None:
      self.pe=pe
      self.g=g
      self.vmap={}
      self.ginit=False
      self.is_add_entity_reference=True
      self.is_add_complex_component=True

  def fill(self,row, level=1) :
       rrow=self.pe.pump(row,level=level) 
       uniq={}
       nrow=[]
       for rr in rrow:
         for ent in rr:  
           if ent.pk not in uniq.keys():
             nrow.append(ent)
             uniq[ent.pk]=1

       return nrow

  def subGraphControlsExpressionWithTemplateReac(self,matches,tag):
    logger.debug(">>>>>>>>>>>>>>>subGraphControlsExpressionWithTemplateReac<<<<<<<<<<<<<<<<<<<<" )

    if matches is None:
        return
    for row in matches:
     row=self.fill(row)   
     for ent in row:
        #k, v = ent
        if isTagged(ent,"Control"):
            control = ent
            ##Control / Catalysis /Modulation /TemplateReactionRegulation
            if isPKorInstance(control, TemplateReactionRegulation):
                o = control
                # print("|TemplateReactionRegulation:" + o.getDisplayName() + "")
            else:
                # print("|" + control.getDisplayName() + "")
                pass
############### here
            colr = control.get_controller()
            controlType = control.get_controlType()

            # activation, inhibition, inhibition-allosteric, inhinition-competitive, inhibition-irreversible,
            # inhibition-noncompetitive, inhibition-other, inhibition-uncompetitive, activation-nonallosteric, activation-allosteric

            # String s=""
            clA = []

            for c in colr:
                validEntity = False
                if isPKorInstance(c, Protein):
                    o = c
                    # s += o.getDisplayName()
                    validEntity = True
                elif isPKorInstance(c, Complex):
                    o = c
                    # s += o.getDisplayName()
                    validEntity = True

                if not validEntity:
                    # raise Exception("unexpected entity " + c.__class__ + "," + c)
                    pass

                # node a, case R1
                ncontroller_A = self.addSpaimNode( c, R1)
                clA.append(ncontroller_A)

            ncontrl_R1 = None
            if CONTROL_VERSION == 1:
                ncontrl_R1 = self.addSpaimNode( control, R1)  # reaction R1 a activ/inhib b
                ncontrl_R1.getSpaimCase().append(R1)
                ncontrl_R1.getPatternTag().append(R1_CONTROLSEXPRESSIONWITHTEMPLATEREAC)

                for ncontrollerA in clA:
                    ct = SpaimEnum.controlTypeToSpaimCodeValidated(control, controlType)

                    if ct is not None:  # a or i
                        e = self.addSpaimEdge( ncontrollerA, ncontrl_R1, R1, ct)

            controlled = control.getControlled()

            # String s=""
            for ctrled in controlled:
                validEntity = False
                if isPKorInstance(ctrled, Interaction):
                    controlledTemplateReact = None
                    if isPKorInstance(ctrled, TemplateReaction):
                        validEntity = True
                        controlledTemplateReact = ctrled  # BiochemicalReaction|ComplexAssembly|Transport
                    elif isPKorInstance(ctrled, BiochemicalReaction):
                        validEntity = False
                    elif isPKorInstance(ctrled, ComplexAssembly):
                        validEntity = False
                    elif isPKorInstance(ctrled, Transport):
                        validEntity = False

                    if not validEntity:
                        if EXIT_ON_ISSUE:
                            raise Exception("subGraphControlsExpressionWithTemplateReac: unexpected entity " + str(ctrled.__class__) + "," + str(ctrled))
                        else:
                            print("subGraphControlsExpressionWithTemplateReac: unexpected entity " + str(ctrled.__class__) + "," + str(ctrled))

                    if CONTROL_VERSION == 1:
                        if controlledTemplateReact is not None:
                            # co: intermediate reaction :
                            # we use co products direcly
                            outp = controlledTemplateReact.getProduct()

                            for ou in outp:
                                np = self.addSpaimNode( ou, R1)
                                ep = self.addSpaimEdge( ncontrl_R1, np, R1, SpaimEnum.product)
                                # logger.debug( "  >>>>out:"+ou.getDisplayName()+"")
                """
                # intermediate reaction : not in R1 case but added independently in graph to increase graph size
                """

                # TODO check if need to reuse rnode
                controlledReactionNode = self.processToSubGraph(controlledTemplateReact, R3, SUP + R1_CONTROLSEXPRESSIONWITHTEMPLATEREAC)

                if CONTROL_VERSION == 2:
                    print("V2")
                    if controlledReactionNode is not None:
                        # -----------------V2--------------do not add control as node  ,but controller to controlled reaction---------------------------
                        print("@=@subGraphControlExpressionWithTemplateReaction=   === add controller to controlled reaction ")

                        for ncontrollerA in clA:
                            ct = SpaimEnum.controlTypeToSpaimCodeValidated(control, controlType)

                            if ct is not None:  # a or i
                                e = self.addSpaimEdge( ncontrollerA, controlledReactionNode, R1, ct)
                                e.setDebug("CETR")


# In[5]:


  def subGraphControlsExpressionWithConversion(self, matches,tag):
    logger.debug(">>>>>>>>>>>>>>>subGraphControlsExpressionWithConversion<<<<<<<<<<<<<<<<<<<<" )
  
    if matches is None:
        return
    for row in matches:
     row=self.fill(row)
     for ent in row:
        #k, v = ent
        if isTagged(ent,"Control"):
            control = ent
     

            if control is None:
                # System.out.print("@@NULL control")
                pass

            if isPKorInstance(control, TemplateReactionRegulation):
                o = control
                # System.out.print( "|TemplateReactionRegulation:" + o.getDisplayName() + "")
            else:
                # System.out.print( "|"+control.getDisplayName()+"");
                pass

            # System.out.print( "|1|"+control.getUri()+"");
            # System.out.print("\n");

            colr = control.get_controller()
            controlType = control.getControlType()

            clA = []

            for c in colr:
                validEntity = False
                if isPKorInstance(c, Protein):
                    o = c
                    validEntity = True
                elif isPKorInstance(c, Complex):
                    o = c
                    validEntity = True

                if not validEntity:
                    if EXIT_ON_ISSUE:
                        raise Exception("subGraphControlsExpressionWithConversion: unexpected entity " + str(c.__class__) + "," + str(c))
                    else:
                        print("subGraphControlsExpressionWithConversion: unexpected entity " + str(c.__class__) + "," + str(c))
                else:
                    ncontroller_A = self.addSpaimNode(c, R1)
                    if ncontroller_A is not None:
                        clA.append(ncontroller_A)

            ncontrl_R1 = None
            if CONTROL_VERSION == 1:
                ncontrl_R1 = self.addSpaimNode( control, R1)  # reaction R1 a activ/inhib b
                ncontrl_R1.getSpaimCase().append(R1)
                ncontrl_R1.getPatternTag().append(R1_CONTROLSEXPRESSIONWITHCONVERSION)

                for ncontrollerA in clA:
                    ct = SpaimEnum.controlTypeToSpaimCodeValidated(control, controlType)
                    if ct is not None:  # ct=a or i
                        e = self.addSpaimEdge( ncontrollerA, ncontrl_R1, R1, ct)

            col = control.getControlled()

            for c in col:
                validEntity = False
                tograph = True
                if isPKorInstance(c, Conversion):
                    co = c
                    if isPKorInstance(c, BiochemicalReaction):
                        validEntity = True
                    elif isPKorInstance(c, ComplexAssembly):
                        validEntity = True
                    elif isPKorInstance(c, Transport):
                        validEntity = True
                        tograph = False
                    elif isPKorInstance(c, Degradation):
                        validEntity = True
                    elif isPKorInstance(c, Conversion):
                        validEntity = True
                        tograph = False
                        # must be added to graph ???

                    if not validEntity:
                        if EXIT_ON_ISSUE:
                            raise Exception("subGraphControlsExpressionWithConversion:2: unexpected entity " + str(c.__class__) + "," + str(c))
                        else:
                            print("subGraphControlsExpressionWithConversion:2: unexpected entity " + str(c.__class__) + "," + str(c))
                    else:
                        if tograph:
                            if CONTROL_VERSION == 1:
                                outp = co.getRight()
                                for ou in outp:
                                    np = self.addSpaimNode( ou, R1)
                                    ep = self.addSpaimEdge( ncontrl_R1, np, R1, SpaimEnum.product)
                                    # logger.debug( "  >>>>out:"+ou.getDisplayName()+"")

                        logger.debug("  >>>>processToSubGraph:start")

                        # TODO check if need to reuse rnode
                        controlledReactionNode = processToSubGraph(co, R3, SUP + R1_CONTROLSEXPRESSIONWITHCONVERSION)

                        logger.debug("  >>>>processToSubGraph:end")
                        if CONTROL_VERSION == 2:
                            logger.debug("V2")
                            if controlledReactionNode is not None:
                                # -----------------V2--------------do not add control as node  ,but controller to controlled reaction---------------------------
                                logger.debug("@=@subGraphControlExpression=   === add controller to controlled reaction ")

                                for ncontrollerA in clA:
                                    ct = SpaimEnum.controlTypeToSpaimCodeValidated(control, controlType)
                                    if ct is not None:  # ct=a or i
                                        e = self.addSpaimEdge(ncontrollerA, controlledReactionNode, R1, ct)
                                        e.setDebug("CE")


# In[6]:


  def process_to_subgraph(self,c,cas, patc):
    spaimGraph=self.g
    if c is None:
        logger.error("<<<<<processToSubGraph-:null-process>>")
        return None

    logger.debug("<<<<<processToSubGraph>>")
    rnode = None
    valid_entity = False
    tograph = True
    msg = ""

    if isPKorInstance(c, Interaction) or isPKorInstance(c, Conversion):
        if isPKorInstance(c, Conversion):
            co = c
            if isPKorInstance(c, BiochemicalReaction):
                valid_entity = True
            elif isPKorInstance(c, ComplexAssembly):
                valid_entity = True
            elif isPKorInstance(c, Conversion):
                valid_entity = True
                tograph = False
                msg = " TODO Conversion"
            elif isPKorInstance(c, Transport):
                tograph = False
                valid_entity = True
                msg = " TODO Transport"
            elif isPKorInstance(c, Degradation):
                tograph = False
                valid_entity = True
                msg = " TODO Degradation"
            elif isPKorInstance(c, Conversion):
                valid_entity = True
                tograph = False
                # must be added to graph ???

            if not valid_entity:
                if EXIT_ON_ISSUE:
                    raise Exception(f"processToSubGraph: unexpected entity {c.__class__},{c}")
                else:
                    print(f"processToSubGraph: unexpected entity {c.__class__},{c}")

            else:
                if tograph:
                    rnode = self.add_spaim_node(co, cas)

                    inl = co.getLeft()
                    for inp in inl:
                        n = self.add_spaim_node( inp, cas)

                        e = self.add_spaim_edge( n, rnode, cas, SpaimEnum.substrate)

                        n.get_spaim_case().append(cas)
                        n.get_pattern_tag().append(patc)

                    outp = co.getRight()
                    for ou in outp:
                        np = self.add_spaim_node( ou, cas)

                        ep = self.add_spaim_edge( rnode, np, cas, SpaimEnum.product)

                        np.get_spaim_case().append(cas)
                        np.get_pattern_tag().append(patc)

                else:
                    logger.info(f"**WARNING** {c} excluded from graph, {msg}")

        if isPKorInstance(c, TemplateReaction):
            valid_entity = True

            co = c
            rnode = self.add_spaim_node( co, cas)

            inp = co.getTemplate()
            if inp is not None:
                n = self.add_spaim_node( inp, cas)

                e = self.add_spaim_edge( n, rnode, cas, SpaimEnum.substrate)

                n.get_spaim_case().append(cas)
                n.get_pattern_tag().append(patc)

            outp = co.getProduct()
            for ou in outp:
                np = self.add_spaim_node( ou, cas)

                ep = self.add_spaim_edge( rnode, np, cas, SpaimEnum.product)

                np.get_spaim_case().append(cas)
                np.get_pattern_tag().append(patc)

    if not valid_entity:
        if EXIT_ON_ISSUE:
            raise Exception(f"{msg},processToSubGraph, unexpected entity {c.__class__},{c}")
        else:
            print(f"{msg},processToSubGraph, unexpected entity {c.__class__},{c}")

    return rnode


# In[7]:


  def subGraphControlPhospho(self, matches,tag):
    spaimGraph=self.g
    logger.debug(
        f">>>>>>>>>>>>>>>subGraphControlPhospho<<<<<<<{tag}<<<<<<<<<<<<<"
    )
    if matches is None:
        return
    
    return self.subGraphControl( matches,tag, tag+"_CONTROLSPHOSPHORYLATION")


  def subGraphControlsMetabolicCatalysis(self, matches,tag):
    logger.debug(
        f">>>>>>>>>>>>>>>subGraphControlsMetabolicCatalysis<<<<<<<{tag}<<<<<<<<<<<<<"
    )
    spaimGraph=self.g
    if matches is None:
        return
    return self.subGraphControl( matches, tag, tag+"_CONTROLSMETABOLICCATALYSIS")


  def subGraphControl(self,matches,spaim_c, patc):
    logger.debug(">>>>>>>>>>>>>>>subGraphControl<<<<<<<<<<<<<<<<<<<<<")
    spaimGraph=self.g
    msg = ""

    if matches is None:
        return
    for row in matches:
     
     row=self.fill(row) 
     
     for ent in row:
        #k, v = ent
        if isTagged(ent,"Control"): 
            print("----------------------->Control")
            ##
            control = ent
            logger.debug("****** match"+control)
            if control is None:
            
                lmsg="@@NULL control"
            else:
                logger.debug(f"== control:{control.__class__.__name__}")

            if isPKorInstance(control, TemplateReactionRegulation):
                o = control
                # System.out.print( "|TemplateReactionRegulation:"+o.getDisplayName()+"");
            else:
                # System.out.print( "|"+control.getDisplayName()+"");
                pass
            # System.out.print( "|3|"+control.getUri()+"");
            # System.out.print("\n")

            colr = control.get_controller()
            control_type = control.get_controlType()

            cl_a = []
            for c in colr:
                logger.debug(f"= controller:{c.getName().toString()}")
                valid_entity = False
                if isPKorInstance(c, Protein):
                    o = c
                    valid_entity = True
                elif isPKorInstance(c, Complex):
                    o = c
                    valid_entity = True
                elif isPKorInstance(c, PhysicalEntity):
                    o = c
                    valid_entity = True

                if not valid_entity:
                    if EXIT_ON_ISSUE:
                        raise Exception(f"subGraphControl: unexpected entity {c.__class__},{c}")
                    else:
                        print(f"subGraphControl: unexpected entity {c.__class__},{c}")
                else:
                    logger.debug("***C1")
                    ncontroller_e = self.add_spaim_node( c, R2)
                    cl_a.append(ncontroller_e)

            valid_entity_c = False
            if (
                control.__class__.isAssignableFrom(Catalysis)
                or isPKorInstance(control, Catalysis)
            ):
                valid_entity_c = True
            elif (
                control.__class__.isAssignableFrom(TemplateReactionRegulation)
                or isPKorInstance(control, TemplateReactionRegulation)
            ):
                valid_entity_c = True
            elif (
                control.__class__.isAssignableFrom(Modulation)
                or isPKorInstance(control, Modulation)
            ):
                valid_entity_c = True

            if not valid_entity_c:
                if EXIT_ON_ISSUE:
                    raise Exception(
                        f"subGraphControl2: unexpected entity {control.__class__},{control}, {msg}"
                    )
                else:
                    print(
                        f"subGraphControl2: unexpected entity {control.__class__},{control}, {msg}"
                    )
            else:
                for ctrled in control.getControlled():
                    valid_entity = False
                    tograph = True
                    if (
                        ctrled.__class__.isAssignableFrom(Conversion)
                        or isPKorInstance(ctrled, Conversion)
                    ):
                        convers = ctrled
                        if isPKorInstance(ctrled, BiochemicalReaction):
                            valid_entity = True
                        elif isPKorInstance(ctrled, ComplexAssembly):
                            valid_entity = True
                        elif isPKorInstance(ctrled, Transport):
                            valid_entity = True
                            tograph = False
                        elif isPKorInstance(ctrled, Degradation):
                            valid_entity = True

                    logger.debug("****")
                    controlled_reaction_node = process_to_subgraph(
                        convers, R3, SUP + patc
                    )

                    if CONTROL_VERSION == 1:
                        logger.debug("   === add control as node ")
                        ncontrol_r2 = self.add_spaim_node( control, R2)
                        ncontrol_r2.get_spaim_case().append(spaim_c)
                        ncontrol_r2.get_pattern_tag().append(patc)

                        for ncontroller_e in cl_a:
                            ct = SpaimEnum.control_type_to_spaim_code_validated(
                                control, control_type
                            )
                            if ct is not None:  # a or i
                                e = self.add_spaim_edge(
                                   ncontroller_e, ncontrol_r2, R2, ct
                                )

                        if valid_entity == False:
                            if EXIT_ON_ISSUE == True:
                                raise Exception(
                                    f"subGraphControl:3: unexpected entity {ctrled.__class__},{ctrled}"
                                )
                            else:
                                print(
                                    f"subGraphControl:3: unexpected entity {ctrled.__class__},{ctrled}"
                                )
                        else:
                            if tograph == True:
                                inp = convers.getLeft()
                                for in_ in inp:
                                    logger.debug("%%%co-subs")
                                    np = self.add_spaim_node( in_, R2)

                                    ep = self.add_spaim_edge(
                                         np, ncontrol_r2, R2, SpaimEnum.substrat
                                    )

                            outp = convers.getRight()
                            for ou in outp:
                                logger.debug("%%%co-prod")

                                np = self.add_spaim_node( ou, R2)

                                ep = self.add_spaim_edge(
                                    ncontrol_r2, np, R2, SpaimEnum.product
                                )

                    if CONTROL_VERSION == 2:
                        logger.debug("V2")
                        if controlled_reaction_node is not None:
                            logger.debug(
                                "@=@subGraphControl=== add controller to controlled reaction "
                            )

                            for ncontroller_e in cl_a:
                                ct = SpaimEnum.control_type_to_spaim_code_validated(
                                    control, control_type
                                )
                                if ct is not None:  # a or i
                                    e = self.add_spaim_edge(
                                        ncontroller_e,
                                        controlled_reaction_node,
                                        R2,
                                        ct,
                                    )
                                    logger.debug(
                                        f"addSpaimEdge   {ncontroller_e.get_label()} --{ct}-->{controlled_reaction_node.get_label()}"
                                    )
                                    e.set_debug("C")

                    logger.debug("            >>>>subGraphControl:END<<<<<<<<<<<<<<<<<<<<")


 


# In[8]:


  def subGraphReactWith(self,matches,tag,subtag):
    logger.debug(">>>>>>>>>>>>>>>>>>>>>>>>>>>>subGraphReactWith<<<<<<<<<<<<<<<<<<<<")
    self.subGraphSp(matches,tag, subtag)


  def subGraphSp(self, matches, cat, pc):
    logger.debug("       >>>>>>>>>>>>>>>>>>>subGraphSp<<<<<<<<<<<<<<<<<<<<")
    if matches is None:
        return
        
    for row in matches:
            lpe = None
            rpe = None
            co = None   
            row=self.fill(row)
            for ent in row:
              print("-------->",ent)              
              if isTagged(ent,"SMR1",silent=False):
                lpe=ent
              elif isTagged(ent,"SMR2",silent=False):  
                rpe=ent
              elif isTagged(ent,"Conv",silent=False):  
                co=ent
                  
            ##TODO: pump_here      
            no = self.add_spaim_node( co, cat)
            no.get_spaim_case().append(cat)
            no.get_pattern_tag().append(pc)

            inp = co.getLeft()
            for in_ in inp:
                n = self.add_spaim_node( in_, cat)
                e = self.add_spaim_edge( n, no, cat, SpaimEnum.substrat)

            outp = co.getRight()
            for ou in outp:
                n = self.add_spaim_node( ou, cat)
                e = self.add_spaim_edge( no, n, cat, SpaimEnum.product)
 

  def subGraphInComplexWithOrInSameComplex(self, matches,tag, subtag):
    logger.debug(">>>>>>>>>>>>>>>>>>>>>>>>>>>>subGraphInComplexWithOrInSameComplex<<<<<<<<<<<<<<<<<<<<")
    if matches is None:
        return    
    for row in matches:
            complx = None
            
            row=self.fill(row) 
                          
            for ent in row:
              if isTagged(ent,"COMPLEX",False):

                complx=ent
                if complx is not None:
                   
                   self.sub_graph_complex(complx, tag, subtag, None)



  def fetch_controller(self, ent ):
       
      ret=[]
     
      p= control_by_controlled(ent)
      matches = self.pe.executePattern(p)
      logger.debug("=>>fetch_controller %s  %s %s " %( len(matches), ent.__class__,ent.pk)) 
      if matches is not  None:
        
        for row in matches:
            inter = None
            
            row=self.fill(row) 
                          
            for ent in row:
              if isTagged(ent,"CONTROL",False):
                logger.debug("=>>CONTROL")
                inter=ent
                if inter is not None: 
                  if inter.get_controller() is not None:
                    logger.debug("=>>CONTROLLER")
                    ret.append(inter.get_controller())  
      
      return ret
  def fetch_control(self, ent ):
      
     
      ret=[]
      p=control_by_controlled(ent)
      matches = self.pe.executePattern(p)
 
      logger.debug("=>>fetch_control %s  %s %s " %( len(matches), ent.__class__,ent.pk))
      if matches is not  None:
        
        for row in matches:
            inter = None
            
            row=self.fill(row) 
                          
            for ent in row:
              if isTagged(ent,"CONTROL",False):
                inter=ent
                if inter is not None: 
                    logger.debug("=>>CONTROL")
                    ret.append(inter)  
      
      return ret
  

  
  def sub_graph_complex(self, complx, cat, pattern_case, def_complxn=None):
    logger.debug("      >>>>>>>>>>>>>>>subGraphComplex")
    #
    spaimGraph=self.g
    complxn =None
    if def_complxn is not None:
        complxn=def_complxn
    else:
       logger.debug("      >>>>>>>>>>>>>>>      %s " %( complx.__class__.__name__))
       logger.debug("              subGraphComplex  --  cat  %s" % ( cat))
       self.add_spaim_node( complx, cat, False)      

    #complxn = def_complxn if def_complxn is not None else self.add_spaim_node( complx, cat, False)

    #logger.debug(" !!   complx ?? get_controller()-> fetch_controller  %s" % ( complx))
    #logger.debug(" !!desc:   %s" % ( describeEntity(complx)))
    
    ctrll = self.fetch_control(complx)
    as_control = None
    if ctrll and len(ctrll) > 0:
        as_control = True
        # R2
        for cont in ctrll:
            logger.debug("         >>>>>>>>>>>>>>>cont",cont)
            control_type = cont.get_controlType() ##???
            col = cont.get_controlled()
            logger.debug("           >>>>>>>>>>>>>>>col",col)
            complex_assembly_node = None
            for c in col:
                valid_entity = False
                if isPKorInstance(c, Conversion):
                    controlled_conv = c
                    if isPKorInstance(c, BiochemicalReaction):
                        valid_entity = False
                    elif isPKorInstance(c, ComplexAssembly):
                        valid_entity = True
                    if valid_entity:
                        complex_assembly_node = self.add_spaim_node(controlled_conv, cat, False)
                        complex_assembly_node.get_spaim_case().add("R2COMPLX")
                        complex_assembly_node.get_pattern_tag().add(pattern_case)
                        # TODO RV
                        e0 = self.add_spaim_edge( complex_assembly_node, complxn, cat, SpaimEnum.product)

                        # redundant
                        inp = controlled_conv.get_left()
                        for i in inp:
                            n = self.add_spaim_node( i, cat, False)
                            e = self.add_spaim_edge( n, complex_assembly_node, cat, SpaimEnum.substrate)
                        outp = controlled_conv.get_right()
                        for o in outp:
                            n = self.add_spaim_node( o, cat, False)
                            e = self.add_spaim_edge( complex_assembly_node, n, cat, SpaimEnum.product)

    else:
      if complxn is not None:  
        as_control = False
        # R3
        ca = ComplexAssembly()

        # to fix pk issue
        # +
        l = complxn.get_label()
        if l is None:
            l = complxn.pk
        complex_assembly_node = self.add_spaim_node_impl( ca, cat, False, "assembly of " + l)
        complex_assembly_node.set_pk(complex_assembly_node.get_label() + "|" + complxn.pk)
        if complex_assembly_node:
            complex_assembly_node.get_spaim_case().add("R3COMPLX")
            complex_assembly_node.get_pattern_tag().add(pattern_case)
            inp = complx.get_component()
            for i in inp:
                n = self.add_spaim_node(i, cat, False)
                e = self.add_spaim_edge( n, complex_assembly_node, cat, SpaimEnum.substrate)
            e = self.add_spaim_edge( complex_assembly_node, complxn, cat, SpaimEnum.product)

    logger.debug("<<<subGraphComplex:END")
    
  def find_node_from_pk(self,no):
    fo = None
    npk = no.get_pk()
    if npk in self.vmap:
        fo = self.vmap[npk]
    else:
        alias_list = alias(no.pk_first_part())
        if alias_list:
            for apk in alias_list:
                if apk in self.vmap:
                    fo = self.vmap[apk]

    return fo




# In[9]:


  def add_spaim_edge(self,g, source, target, spaim_tag, spaim):
    e = SpaimEdge()

    if source is not None and target is not None:
        e.set_pk_from_nodes(source, target)

        if e.get_pk() in emap:
            e = emap[e.get_pk()]
        else:
            self.g.add_edge(e, source, target)
            emap[e.get_pk()] = e

        if e.get_spaim_tag() is not None:
            e.set_spaim_tag(e.get_spaim_tag() + ";" + spaim_tag)
        else:
            e.set_spaim_tag(spaim_tag)

    e.set_spaim(spaim)

    logger.debug(
        " !! addSpaimEdge   %s --%s-->%s     %s"
        % (source.get_label(), e.get_spaim(), target.get_label(), e.get_spaim_tag())
    )

    return e

  

  def create_nprop(self,property_name,tpr) :
      
      tp="string"
      #######type check not working:FIXME
      if tpr.__class__==int :
         tp="int" 
      elif   tpr.__class__==float:           
         tp="double"  
      elif   tpr.__class__==bool:           
         tp="bool"    
      ###########FIXME   
      logger.debug("type conversion : %s %s " %(tpr,tp))    

      has_property = property_name in self.g.vertex_properties
      if has_property:
         prop = self.g.vp[property_name]
      else:
         prop = self.g.new_vertex_property(tp)
         self.g.vp[property_name]=prop  
   
      return prop
  def   attribute_dict(self,snode):
      attribute_dict = {attr: type(getattr(snode, attr)) for attr in vars(snode)}
      return attribute_dict
  
  def initgraphp(self,snode):
      
       logger.debug("  @@@initgraphp1 "   )
       if self.ginit ==False:
           logger.debug("  @@@initgraphp2 "   )
           td=self.attribute_dict(snode)
           for property_name,tp in td.items():
                 logger.debug("  @@@property_name,tp : %s %s " %( property_name,tp) )
                 self.create_nprop(property_name,tp)

           self.ginit=True 

  def   add_vertex(self,snode):
       self.initgraphp(snode)  

       attrd = vars(snode)
      
       n = self.g.add_vertex()
       for attrn,vl in attrd.items():
           
         self.g.vp[attrn][n]=vl
         #self.nodeImplByCustomId[nid]=n

  def add_spaim_node(self,  be, cat,rec=True):
    
    return self.add_spaim_node_impl(  be, cat, rec, None)


  def add_spaim_node_impl(self,  be, cat, rec, default_label):
    logger.debug("    --addSpaimNode be   %s" % (be.__class__.__name__))
    logger.debug("               --  cat  %s" % ( cat))
    logger.debug("               --  rec %s " % (rec))
    logger.debug("               --  default_label %s" % ( default_label))
    
    no = SpaimNode()
    no.element=be

    label = None
    if default_label is None:
        label = define_node_label(no)
    else:
        label = default_label

    if label is None:
        return None

    no.set_label(label)
    no.get_safe_label()

    no.set_description(defined_description(be))

    no.set_group("")
 
    if (
        isinstance(be,Entity)
        or isOnlyModelInstance(be, Entity)
    ):
        ent = cast(be, Entity)
        prop = {}
        if (
            isinstance(be,PhysicalEntity)
            or isOnlyModelInstance(be, PhysicalEntity)
        ):
            pe = cast(be, PhysicalEntity)

            if not isOnlyModelInstance(pe, SmallMolecule):
                hgnc = identifiers(pe, "hgnc symbol", False, False)
                if hgnc is not None:
                  no.hgnc= format_str(join(hgnc, ";"))

                uni = identifiers(pe, "uniprot", True, False)
                if uni is not None:
                   no.uniprot=format_str(join(uni, ";"))

            if isOnlyModelInstance(pe, SmallMolecule) or isOnlyModelInstance(pe, PhysicalEntity):
                chebi = identifiers(pe, "chebi", False, False)
                if chebi is not None:
                  no.chebi=format_str(join(chebi, ";"))

            org = [bs.get_displayName() for bs in MUtils.get_organisms(pe)]
            if org is not None:
              no.organism=format_str(join(org, ";"))

        no.uri=format_str(be.pk)
        no.pk=be.pk
        no.generic=MUtils.is_generic(be)#TODO
        prov=[]
        dsl=[]
        dds=ent.get_dataSource()
        if dds is not None:
          if isinstance(dds,list):
             dsl=dds
          else:
             dsl.append(dds) 
        for ds in dsl:
            if ds is not None:
              prov.append(ds.get_displayName())
        
        no.set_provider(format_str(join(prov, ";")))
        if ent.get_name() is not None:
            nm = set(ent.get_name())
        else:
            nm=set()
        nm.add(ent.get_displayName())
        no.alias=format_str(join(nm, ";"))

        no.evidence=format_str(join(ent.get_evidence(), ";"))

        logger.debug(no.label)

    npk = no.get_pk()

    fo = self.find_node_from_pk(no)
    if fo is not None:
        no = fo
    else:
        self.add_vertex(no)
        self.vmap[npk] = no

    if (
        isinstance(be,PhysicalEntity)
        or isOnlyModelInstance(be, PhysicalEntity)
    ):
        ##FIXME :integrate this commented  code
        """
        if self.is_add_entity_reference == True:
            if isinstance(be,SequenceEntity):
                se = cast(be, SequenceEntity)
                er = se.get_entity_reference()
                ern = self.add_spaim_node( er, cat)
                self.add_spaim_edge( ern, no, cat, SpaimEnum.product)

        if self.is_add_complex_component == True and rec == True:
            if isOnlyModelInstance(be, Complex):
                co = cast(be, Complex)
                self.sub_graph_complex(co, "R2_R3?", RX_COMPLEX, no)
        """
    return no


# In[10]:
def attvalue(ent, attn) :
      attv=None
      try: 
             att_getter=getattr(ent,'get_'+attn)
             attval=att_getter()
             ##print(attval) 
             if attval is not None: 
                ##print("not null") 
               attv=attval
      except:
              print("error")
              pass
      return attv

def fetch(ent):
    children=[]
    for attn in ent.object_attributes():
           attval=attvalue(ent, attn)
           if attval is not None:
              children.append(attval) 
    return children

def identifiers(entity, xrefdb, is_prefix, include_evidence):
    ids = set()
    
    children =  fetch(entity)
    children.append(entity)  # include itself

    for child in children:
        if (
            isOnlyModelInstance(child, PhysicalEntity)
            or isOnlyModelInstance(child, EntityReference)
            or isOnlyModelInstance(child, Gene)
        ):
           xf=child.get_xref()
           if xf is not None: 
             xrefl=[]
             if not isinstance(xf,list):
                     xrefl.append(xf)  
             for x in xrefl:
                db=x.get_db()
                if ( db is not None and  xrefdb is not None  and x.get_id() is not None ):
                  if ( (is_prefix and db.lower().startswith(xrefdb.lower()))
                    or xrefdb.lower() == db.lower()
                  ):
                      ids.add(x.get_id())

    return ids



# In[11]:


"""
from bio_pax_elements import (
    BioPAXElement,
    PhysicalEntity,
    Interaction,
    Control,
    EntityReference,
    TemplateReactionRegulation,
    Catalysis,
    TemplateReactionRegulation,
    ComplexAssembly,
    Conversion,
    Stoichiometry,
    Entity,
)

from fetcher import Fetcher
from xreferrable import XReferrable
from xref import Xref
"""

def format_str(s):
    # s = s.replace("&apos;", "&quot;")
    return html.escape(s)


def defined_description(be):
    d = ""
    sep = ";"
    if be:
        if str(be) is not None and str(be) != "null":
            d += str(be) + sep
        if be.pk is not None and be.pk is not None:
            d += str(be) + sep

        if d.endswith(sep):
            d = remove_last_char(d, sep)

    return d


def join(iterabl, sep):

    s = ""
    if iterabl is not None: 
     sp = None
     sz = len(iterabl)
     for i, o in enumerate(iterabl):
        if o is not None and str(o) != "":
            if i + 1 >= sz:
                sp = ""
            else:
                sp = sep
            s += str(o) + sp

     if s.endswith(sep):
        s = remove_last_char(s, sep)

    return s


def remove_last_char(s, sc):
    c = sc[0]
    if s is not None and len(s) > 0 and s[-1] == c:
        s = s[:-1]
    return s





def alias(pk_first_part):
    # TODO: element alias system
    return None


def define_node_label(no):
    be = no.element
    no.simple_uri=be.pk

    luri = False

    label = ""
    if isOnlyModelInstance(be, PhysicalEntity) or isOnlyModelInstance(be, PhysicalEntity):
        o = be
        label = o.get_displayName()
    elif isOnlyModelInstance(be, Interaction) or isOnlyModelInstance(be, Interaction):
        o = be
        label = o.get_displayName()
    elif isOnlyModelInstance(be, Control) or isOnlyModelInstance(be, Control):
        o = be
        label = o.get_displayName()
    elif isOnlyModelInstance(be, EntityReference) or isOnlyModelInstance(be, EntityReference):
        o = be
        label = o.get_displayName()
    elif (
        isOnlyModelInstance(be, TemplateReactionRegulation)
        or isOnlyModelInstance(be, TemplateReactionRegulation)
    ):
        o = be
        label = o.get_displayName()
    else:
        label = str(be)

    if label is None or label == "null":
        if isOnlyModelInstance(be, Catalysis):
            o = be
            label = o.get_displayName()
            if label is None:
                label = o.get_standard_name()

            if label is None:
                label = "CATALYSIS|" + str(be)
        if isOnlyModelInstance(be, TemplateReactionRegulation):
            o = be
            label = o.get_displayName()
            if label is None:
                label = o.get_standard_name()

            if label is None:
                label = "CATALYSIS|" + str(be)
        elif isOnlyModelInstance(be, Interaction) or isOnlyModelInstance(be, Interaction):
            o = be
            label = o.get_displayName()
            if label is None:
                label = o.get_standard_name()

            label = no.simple_uri
            luri = True
        else:
            label = no.simple_uri
            luri = True

    if label is None:
        label = define_labelfx(be)
        luri = True

    if isOnlyModelInstance(be, ComplexAssembly) or isOnlyModelInstance(be, ComplexAssembly):
        if label is None:
            label = define_complex_assembly_id(be, label)

    if luri is True and no.get_biopax_type() is not None and label is not None and label.startswith(
        "http:"
    ) and label.find(no.get_biopax_type()) != -1:
        try:
            uri = urlparse(no.get_uri())
            label = uri.path
        except Exception as e:
            pass

    return label


def define_complex_assembly_id(be, lb):
    joined = None

    o = be

    lb = o.get_displayName()

    plabl = []
    for p in o.get_participant_stoichiometry():
        v = define_labelfx(p)
        if v:
            plabl.append(v)

    for p in o.get_participant():
        v = define_labelfx(p)
        if v:
            plabl.append(v)

    for p in o.get_right():
        v = define_labelfx(p)
        if v:
            plabl.append(v)

    for p in o.get_left():
        v = define_labelfx(p)
        if v:
            plabl.append(v)

    if lb is not None:
        plabl.append(lb)

    plabl.sort(key=len, reverse=True)
    joined = "".join(plabl)
    if joined and joined != "":
        return joined
    else:
        return None


def sort_by_string_length(plabl):
    plabl.sort(key=len, reverse=True)


# In[12]:




def get_interaction_chemical_modifications(participant_element):
    if not participant_element:
        return None

    chemical_modifications_set = set()

    modification_features = get_values(participant_element, "feature")
    if modification_features:
        for modification in modification_features:
            if modification:
                value =  get_value(modification, "modificationType")
                if value:
                    mod = str(value)
                    mod = mod[mod.index("_") + 1 :].replace("[", "").replace("]", "")
                    chemical_modifications_set.add(mod)

    modification_not_features =  get_values(participant_element, "notFeature")
    if modification_not_features:
        for modification in modification_not_features:
            if modification:
                value =  get_value(modification, "modificationType")
                if value:
                    mod = str(value)
                    mod = "!" + mod[mod.index("_") + 1 :].replace("[", "").replace("]", "")
                    chemical_modifications_set.add(mod)

    return chemical_modifications_set


def get_value(bpe, *properties):
    for property in properties:
        try:
            method = getattr(
                bpe.getModelInterface(),
                f"get{property[0].upper()}{property[1:].replace('-', '_')}",
            )
            invoke = method.invoke(bpe)
            if invoke is not None:
                return invoke
        except Exception as e:
            print(e)

    return None


def get_values(bpe, *properties):
    col = set()

    for property in properties:
        try:
            method = getattr(
                bpe.getModelInterface(),
                f"get{property[0].upper()}{property[1:].replace('-', '_')}",
            )
            invoke = method.invoke(bpe)
            if invoke is not None:
                if isOnlyModelInstance(invoke, Collection):
                    col.update(invoke)
                else:
                    col.add(invoke)
        except Exception as e:
            print(e)

    return col


def define_labelfx(element):
    label = define_namefx(element)

    if not isOnlyModelInstance(element, Interaction):
        chemical_modifications_wrapper = get_interaction_chemical_modifications(element)
        if chemical_modifications_wrapper is not None:
            label += str(chemical_modifications_wrapper)

        if isOnlyModelInstance(element, PhysicalEntity):
            cl = element.getCellularLocation()
            if cl:
                terms = str(cl)  # It's like CellularLocationVocabulary_terms...
                terms = terms[terms.index("_") + 1 :].replace("[", "").replace("]", "")
                label += "; " + terms if len(terms) > 0 else ""

    return label

### ???
def define_namefx(bpe):
    node_name = None
    if isOnlyModelInstance(bpe, Named):  # what is Named ???
        node_name = bpe.getDisplayName()
    r = None
    if node_name is None or not node_name:
        r = bpe.getUri()
    else:
        r = html.unescape(node_name)

    return r

class MUtils():
  #TODO : implement this
  @staticmethod 
  def get_organisms(pe):
      return []
  @staticmethod
  def is_generic(be):
      return False
  
#def normalize(m):
#    ModelUtils.merge_equivalent_interactions(m)
#    ModelUtils.normalize_generics(m)  # TODO not sure want to apply this...
#    for spe in set(m.getObjects(SimplePhysicalEntity)):
#        ModelUtils.add_missing_entity_reference(m, spe)
#        uri = urlparse(spe.getUri())
#        # logger.info(" "+spe.getDisplayName()+" ---- "+uri.getFragment())


# In[ ]:





# In[ ]:





# In[13]:




class SpaimNode:
    def __init__(self):
        self.simple_uri = None
        self.numid = None
        self.hgnc = None
        self.uri = None
        self.chebi = None
        self.generic = None
        self.organism = None
        self.uniprot = None
        self.alias = None
        self.provider = None
        self.evidence = None
        self.export = False
        self.safeLabel = None
        self.label = None
        self.description = None
        self.shape = None
        self.color = None
        self.group = None
        self.debug = None
        self.srctype = None
        self.input = None
        self.biopaxType = None
        self.spaimCaseFlat = None
        self.patternTagFlat = None
        self.pk = None
        self.map = {}
        self.element = None
        self.spaimCase = set()
        self.patternTag = set()
        self.dbid = None
        self.reversible = None

    def default_val(self):
        if self.pk is None:
            self.pk = ""
        if self.dbid is None:
            self.dbid = ""
        if self.reversible is None:
            self.reversible = ""
        if self.patternTagFlat is None:
            self.patternTagFlat = ""
        if self.debug is None:
            self.debug = ""
        if self.spaimCaseFlat is None:
            self.spaimCaseFlat = ""
        if self.biopaxType is None:
            self.biopaxType = ""
        if self.input is None:
            self.input = 0
        if self.group is None:
            self.group = ""
        if self.srctype is None:
            self.srctype = ""
        if self.simple_uri is None:
            self.simple_uri = ""
        if self.safeLabel is None:
            self.safeLabel = ""
        if self.label is None:
            self.label = ""
        if self.description is None:
            self.description = ""
        if self.shape is None:
            self.shape = ""
        if self.color is None:
            self.color = ""
        if self.group is None:
            self.group = ""

    def get_export(self):
        return self.export

    def set_export(self, export):
        self.export = export

    def get_evidence(self):
        return self.evidence

    def set_evidence(self, evidence):
        self.evidence = evidence

    def get_alias(self):
        return self.alias

    def set_alias(self, alias):
        self.alias = alias

    def get_provider(self):
        return self.provider

    def set_provider(self, provider):
        self.provider = provider

    def get_reversible(self):
        return self.reversible

    def set_reversible(self, reversible):
        self.reversible = reversible

    def get_color(self):
        return self.color

    def set_color(self, color):
        self.color = color

    def get_biopax_type(self):
        return self.biopaxType

    def set_biopax_type(self, biopax_type):
        self.biopaxType = biopax_type

    def get_input(self):
        return self.input

    def set_input(self, input_value):
        self.input = input_value

    def get_shape(self):
        return self.shape

    def set_shape(self, shape):
        self.shape = shape

    def get_description(self):
        return self.description

    def set_description(self, description):
        self.description = description

    def get_label(self):
        return self.label

    def set_label(self, label):
        self.label = label

    def get_group(self):
        return self.group

    def set_group(self, group):
        self.group = group

    def get_numid(self):
        return self.numid

    def set_numid(self, numid):
        self.numid = numid

    def get_safe_label(self):
        if self.label is None:
            self.label = ""
        if self.safeLabel is None:
            self.set_safe_label(self.label)
        return self.safeLabel

    def set_safe_label(self, safe_label):
        self.safeLabel = safe_label

    def get_spaim_case(self):
        return self.spaimCase

    def get_spaim_case_flat(self):
        sep = "_"
        return sep.join(self.spaimCase)

    def set_spaim_case(self, spaim_case):
        self.spaimCase = spaim_case

    def get_pattern_flat(self):
        sep = "_"
        return sep.join(self.patternTag)

    def get_pattern_tag(self):
        return self.patternTag

    def get_safe_description(self):
        return self.description

    def set_pattern_tag(self, pattern_tag):
        self.patternTag = pattern_tag

 


    @staticmethod
    def random_key():
        unique_id = uuid.uuid4()
        return str(unique_id.variant)


    def pk_first_part(self):
        return self.label

    def pk_second_part(self):
        return type(self.element).__name__

    def get_pk(self):
        if self.pk is None or self.pk=="" :
            pk = self.pk_second_part()
            if self.pk_first_part() is not None:
                pk = self.pk_first_part() + "|" + self.pk
            logger.logger("@@WARNING: inconsistent NodePK - no uri   defined, using random id for safe generation")
            pk = self.random_key() + "|" + pk

            self.pk=pk 

        return self.pk
    



# In[14]:


from typing import Dict

class SpaimEdge:
    def __init__(self):
        self.numid = None
        self.interaction = ""
        self.source = ""
        self.debug = ""
        self.target = ""
        self.pk = ""
        self.map = {}
        self.label = ""
        self.spaim = ""
        self.spaimTag = ""
        self.export = False

    def default_val(self):
        if self.pk is None:
            self.pk = ""
        if self.label is None:
            self.label = ""
        if self.spaim is None:
            self.spaim = ""
        if self.spaimTag is None:
            self.spaimTag = ""
        if self.debug is None:
            self.debug = ""
        if self.interaction is None:
            self.interaction = ""

    def get_export(self):
        return self.export

    def set_export(self, export):
        self.export = export

    def get_spaim_tag(self):
        return self.spaimTag

    def set_spaim_tag(self, spaim_tag):
        self.spaimTag = spaim_tag

    def get_spaim(self):
        if self.spaim is None:
            return ""
        return self.spaim

    def set_spaim(self, spaim):
        self.spaim = spaim

    def get_source(self):
        return self.source

    def set_source(self, source):
        self.source = source

    def get_target(self):
        return self.target

    def set_target(self, target):
        self.target = target

    def get_numid(self):
        return self.numid

    def set_numid(self, numid):
        self.numid = numid

    def get_interaction(self):
        return self.interaction

    def set_interaction(self, interaction):
        self.interaction = interaction

    def set_pk_from_nodes(self, source, target):
        self.pk = f"{source.get_pk()}-->{target.get_pk()}"

    #def get_pk(self):
    #    return self.pk

    def set_pk(self, pk):
        self.pk = pk

    def get_map(self):
        return self.map

    def set_map(self, map):
        self.map = map

    def get_label(self):
        return self.label

    def set_label(self, label):
        self.label = label

    def __str__(self):
        return f"\nSpaimEdge [pk={self.pk}, Label={self.label}]\n"

    def set_debug(self, debug):
        self.debug = debug

    def get_debug(self):
        return self.debug


# In[15]:


from typing import List
#from directed_sparse_multigraph import DirectedSparseMultigraph  # ement or replace with actual class
#from spaim_node import SpaimNode  # ement or replace with actual class
#from spaim_edge import SpaimEdge  # ement or replace with actual class


class RawPatternSearchResult:
    
    def __init__(self, report: str, graph_list: List):
    #def __init__(self, report: str, graph_list: List[DirectedSparseMultigraph[SpaimNode, SpaimEdge]]):
        self.report = report
        self.graph_list = graph_list

 

class SpaimEnum:

    substrat = "s"
    product = "p"
    activator = "a"
    inhibitor = "i"
    modulator = "m"
    undefined = None

    @staticmethod
    def control_type_to_spaim_code(control, control_type):
        ct = control_type.toString().upper()

        if ct.startswith("ACTIVATION"):
            return SpaimEnum.activator
        elif ct.startswith("INHIBITION"):
            return SpaimEnum.inhibitor
        else:
            print("WARNING: unhandled controlType for control " + control.getDisplayName() + " controlType:" + control_type)
            return SpaimEnum.undefined

    @staticmethod
    def control_type_to_spaim_code_validated(control, control_type):
        if control_type is None:
            print("WARNING: null controlType for control " + control.getDisplayName() + " controlType:" + control_type)
            return SpaimEnum.undefined

        return SpaimEnum.control_type_to_spaim_code(control, control_type)



# In[16]:



 
def readBinDataFromLabel(label) :
    file="data/%s.pkl"%(label)
    data=None
    with open(file, "rb") as f:
       data = pickle.load(f)
    return data

def writeBinDataFromLabel(data,label) :
    file="data/%s.pkl"%(label)
    #with open(file, 'w') as f:
    f = open(file, 'wb')
    pickle.dump(data, f)
    filejson="data/%s.json"%(label)
    #with open(filejson, 'w') as f:
    #   json.dump(data, f)
    return file

#execute pattern with option to cache query result in a file
def executePattern(pe,p,label,rcache=False):
      


    
      logger.debug("pattern-label : %s"%(label))
      data=None
      if rcache==True:
        data=readBinDataFromLabel(label)
      else:
        logger.debug("running query")
        data=pe.executePattern(p)
        writeBinDataFromLabel(data,label) 
      return data     
          

def rawPatternSearch(pe, blacklist=None,do_process=True,cache=False):

        spaimGraph=Graph(directed=False)
        
        rp=RegulationProcessor(pe,spaimGraph)

        R1="R1"
        R2="R2"
        R3="R3"
        R3_REACTSWITH = "R3_REACTSWITH"  # Define your constants if not already defined
        R3_PRODUCE="R3_PRODUCE"
        RX_INCOMPLEXWITH = "RX_INCOMPLEXWITH"
        RX_INSAMECOMPLEX = "RX_INSAMECOMPLEX"
        #SpaimGraphGenerator.normalize(m)

        p = None
        matches = None
        report = ""
        glist = []
        rk=Rack()
        r1ct = -1
        r1ct1 = -1
        r1ct2 = -1
        rphospho = -1
        r2ct = -1
        rcomplx1 = -1
        rcomplx2 = -1
        r3ct = -1
        r3ct1 = -1
        r3ct2 = -1
        logger.info("**************CASE_R1*******************")
        label="CASE_R1_1"
        logger.info("**************%s*******************" %(label))
        p = rk.controlsExpressionWithTemplateReac()
        p.blacklist=blacklist
        matches = executePattern(pe,p,label,cache)
        if do_process:
          r1ct1 = count_pattern_search_result( matches)
          rp.subGraphControlsExpressionWithTemplateReac( matches,R1)

        label="CASE_R1_2"
        logger.info("**************%s*******************" %(label))
        p = rk.controlsExpressionWithConversion()
        p.blacklist=blacklist
        matches = executePattern(pe,p,label,cache)
        if do_process:
          r1ct2 =count_pattern_search_result( matches)
          r1ct = {**r1ct1, **r1ct2}  
          rp.subGraphControlsExpressionWithConversion( matches, R1)

        label="CASE_R1_3"
        logger.info("**************%s*******************" %(label))
        p = rk.controlsPhosphorylation()
        p.blacklist=blacklist
        matches = executePattern(pe,p,label,cache)
        if do_process:
          rphospho = count_pattern_search_result( matches)
          rp.subGraphControlPhospho(matches, R1)
    
        logger.info("**************CASE_R2*******************")

        label="CASE_R2_1"
        logger.info("**************%s*******************" %(label))
        p = rk.controlsMetabolicCatalysis()
        p.blacklist=blacklist
        matches = executePattern(pe,p,label,cache)
        if do_process:
          r2ct = count_pattern_search_result( matches)
          rp.subGraphControlsMetabolicCatalysis( matches, R2)

        logger.info("**************CASE_R3*******************")
        label="CASE_R3_1"
        logger.info("**************%s*******************" %(label))
        p = rk.reactsWith()
        p.blacklist=blacklist
        matches = executePattern(pe,p,label,cache)
        
        if do_process:
          r3ct1 = count_pattern_search_result( matches)
          rp.subGraphReactWith(matches, R3, R3_REACTSWITH)

        label="CASE_R3_2"
        logger.info("**************%s*******************" %(label))
        p = rk.usedToProduce()
        p.blacklist=blacklist
        matches = executePattern(pe,p,label,cache)
        if do_process:
          r3ct2 = count_pattern_search_result( matches)
          rp.subGraphReactWith(matches,R3, R3_PRODUCE)
        r3ct = {**r3ct1, **r3ct2}
        
        label="CASE_R3_3"
        logger.info("**************%s*******************" %(label))
        p = rk.inComplexWith()
        p.blacklist=blacklist
        matches = executePattern(pe,p,label,cache)
        if do_process:
          rcomplx1 = count_pattern_search_result( matches)
          rp.subGraphInComplexWithOrInSameComplex(matches, R3, RX_INCOMPLEXWITH)

        label="CASE_R3_4"
        logger.info("**************%s*******************" %(label))
        p = rk.inSameComplex()
        p.blacklist=blacklist
        matches = executePattern(pe,p,label,cache)
        if do_process:
          rcomplx2 = count_pattern_search_result( matches)
          rp.subGraphInComplexWithOrInSameComplex( matches, R3,RX_INSAMECOMPLEX)
            
        
        if do_process:
          report += " R1=" + json.dumps(r1ct)+"\n"
          report += " R2=" + json.dumps(r2ct)+"\n"
          report += " R3=" + json.dumps(r3ct)+"\n"
          report += " RPHOSPHO=" + json.dumps(rphospho)+"\n"
          report += " RCOMPLX1=" + json.dumps(rcomplx1)+"\n"
          report += " RCOMPLX2=" + json.dumps(rcomplx2)+"\n"

          glist.append(rp.g)
          res = RawPatternSearchResult(report, glist)
        return res






