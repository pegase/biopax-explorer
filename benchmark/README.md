## BIOPAX-Explorer benchmarks 

 reproducible benchmarks and comparison with paxtools available at 


[benchmarks repository](https://gitlab.inria.fr/fmoreews/rdfobject/-/tree/master/release/script/bench?ref_type=heads) 

 

## benchmarks how to


datasets are in the data folder
```
data/biopax/reactome_homo_sapiens.owl
data/biopax/reactome_mus_musculus.owl
```
# installation
install Java for paxtools
install Python>=3.8
pip install biopax-explorer
docker-compose up # deploy fuseki + jupyter
# add the datasets in fuseki (reactome_mus, reactome_homo)

# benchmarks scripts
```
bash bench_biopax_explorer.sh
bash bench_paxtools.sh
```
# reports
```
python generate_table_report.py
cat table.json
```
# plots:
```
python generate_metrics.py
```

